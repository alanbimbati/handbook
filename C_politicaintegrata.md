# Integrated policy for Quality, Social Responsibility, Information Security and Business Continuity

Agilelab's strategic mission is to guide companies towards the use of cutting-edge software that leverages artificial intelligence and real-time data insertion. The organization's activities include:  
- Research, study, design, implementation, and trade of computer, telematic, processing, and computation systems.  
- Design and implementation of technological and operational platforms for the management and storage of all types of data, including personal data.  
- Creation, commercial management, and consultancy for "virtual spaces" such as websites, social media platforms, and forums.  
- Agilelab has implemented its Management System in accordance with international standards ISO 9001, SA8000, ISO 27001, ISO 22301, and ISO 27701, ensuring internal efficiency, adequate security levels, worker satisfaction, information security, and personal data protection, better positioning to meet customer needs.  

Agilelab has decided to adopt an integrated policy to uniquely guide the choice of strategies and subsequent activities. The company aims to strengthen its market presence by achieving significant goals ranging from strengthening its image, market knowledge, and the awareness that introducing and using an integrated management system in the company increases the chances of achieving full customer satisfaction through the ability to offer customized products and services capable of innovating the reference market and generating steady and continuous growth, ensuring operational continuity.

In defining its policy, Agilelab considered:

- Full satisfaction of customer expectations as explicitly defined in documents and implicitly in the need to accompany them to the result.  
- Compliance with mandatory requirements, those autonomously established by the company, international regulations, universally accepted principles, and the fundamental standards of the ILO.  
- Continuous improvement of its processes through activities based on verifying their effectiveness, efficiency, and therefore their real applicability in the production context.  
- Risk and opportunity analysis.  
- Personal needs and expectations of partners, administrators, and especially workers.  
- Expected contributions from suppliers.  

The company's primary commitment is to provide a service in line with market expectations, offering innovative services where possible, constantly updated, and in line with the requirements expressed by the customer. Agilelab commits to maintaining a high standard of its services and ensuring the compliance of their requirements. Furthermore, it commits to maintaining a transparency policy, for which it has created a website that allows interested parties to view the features of its services. An essential requirement for the company is the ability to provide efficient and effective services while maintaining the necessary quality in relation to the market-defined price.

Agilelab monitors the satisfaction level of its customers to verify the effectiveness of its processes and trigger corrective actions if necessary. From this perspective, the company commits to selecting suppliers capable of guaranteeing high-quality service. Agilelab commits to analyzing the risks and opportunities it faces, as well as the market in which it operates and competitors, to best manage potential interferences and problems before they arise. Through its IT resources, Agilelab can offer a high-level product and service.

## Information Security

The Management is responsible for protecting and safeguarding against all threats, whether internal or external, intentional or accidental:

The information necessary for Agilelab's business.
Customer information managed during the product and service lifecycle in accordance with the indications provided by Reg. EU 679/2016 and associated standards, ISO 27701, and ISO 27001.
Agilelab's Integrated Management System defines a set of organizational, technical, and procedural measures to ensure compliance with the basic security requirements listed below:

### Confidentiality: 
Information must only be known to those with appropriate privileges.
### Integrity: 
Information can only be modified by those who have the privileges.
### Availability: 
Information must be accessible and usable when requested by processes and users with the relevant privileges. Availability objectives include resilience goals.

This policy defines the principles of information security and personal data that guide:

The behavior of the subjects to whom it is addressed, within the ISMS, and the implementation of processes, procedures, instructions, the adoption of practices, and other controls within the ISMS.

The principles that determine and support the definition and implementation of the ISMS to ensure information security are then expressed.
- All essential service information (e.g., technical and commercial documents, source code, configuration information, service-related emails, customer-provided information, staff data, etc.) must be protected.
- All information to be protected must be managed according to the assigned classification level, in compliance with the relevant procedures, throughout its lifecycle.
- Information security is a fundamental aspect of Agilelab's success and achieving business objectives.
- Maintaining ISO 27001 and ISO 27701 certifications provides tangible, visible, and third-party assessable evidence of Agilelab's commitment to ensuring information security and personal data protection.
- Everyone who comes into contact with protected information in various ways plays a direct role in the success of such protection. It is therefore the direct and explicit responsibility of these subjects to adhere to the principles contained in this policy and all applicable security policies related to it and to ensure compliance.
- Information security is designed and implemented to be an integral part of normal business processes and behaviors and is defined so as not to prejudice the adequacy of the same for the purposes and objectives of the organization.
- Achieving security objectives is governed by a risk-based approach, which involves applying a risk management process that takes into account the organization's context, the scope of the ISMS, and the organization's objectives.
- The organization adopts a structured process for managing information security incidents aimed at containing impacts, identifying causes, and promoting their removal. All subjects affected by the ISMS are required to report any abnormal or suspicious circumstances regarding information.

Agilelab has dedicated competent personnel to:

- Issue all necessary standards, including the type of document classification, so that the company can safely conduct its activities.
- Adopt criteria and methodologies for risk analysis and management.
- Suggest organizational, procedural, and technological security measures to protect the security and operational continuity of activities.
- Periodically check the company's service exposure to major threats.
- Verify security incidents and adopt appropriate countermeasures.
- Promote the culture of information security and personal data protection.
- All external parties who have relationships with Agilelab must ensure compliance with the security requirements set out in this policy, also by signing a "confidentiality agreement" when the assignment is conferred, if not already expressly mentioned in the contract.

The security objectives of Agilelab's information and data are defined in relation to strategic and business objectives, in compliance with contractual commitments and current regulations. Achieving these security objectives is planned, implemented, monitored, and controlled with the support of a specific risk management methodology.

## Business Continuity

The objectives of this Business Continuity Policy can be identified in the following points, shared by all Agilelab locations:

- Ensure operational continuity and minimize business impacts in the event of a crisis, ensuring a rapid return to normal activity.
- Protect Agilelab's interests and increase the trust of its customers and partners, ensuring service continuity to meet the constraints arising from current regulations and contractual obligations, as well as to ensure its reliability towards customers.
- Maintain an effective, real, certified Operational Continuity Management System capable of proving effective in its practical application, in compliance with ISO 22301.

The goal is to minimize compliance verification activities with customer requirements.

The principles that determine and support the definition and implementation of the ISMS to ensure Business Continuity are then expressed:
- The Operational Continuity Management System is defined and maintained according to a delineated process and reviewed on a regular periodic basis to ensure compliance with legal obligations, standards, and recognized best practices.
- Business services and the factors involved in delivery (Personnel, Organization Locations, and ICT Systems) are identified, evaluated for criticality, and documented based on contractual, business, and internal requirements.
- Impact analyses on operational activities (BIA Business Impact Analysis) and risk analyses are carried out on the services provided by the organization that fall within the scope of the management system according to a defined process and on a regular periodic basis.
- The operational continuity management system is direct and consequent to business needs. Business Units play a central role in the strategic and executive definition of this system.
- All personnel must be informed of the existence of the operational continuity policies established in the company, have access to them, and responsibly contribute to their application and improvement.
- Operational continuity plans and disaster recovery plans are defined, documented, and tested on a regular periodic basis to ensure the adequacy and continuous updating of the technical and organizational solutions adopted, as well as training plans for all personnel and entities involved.
- Operational continuity is integrated into change management and the development of new services, ensuring continuous improvement and increasing the organization's resilience to adverse events.
- Third parties and subcontractors with a critical role in service delivery must be aware of and comply with the operational continuity policies adopted.

We commit to continuously adapting and improving our Management System and raising awareness and training our stakeholders about its correct application.
The General Management, based on the data provided by the Quality Manager, annually reviews and updates its policy, objectives, and related indicators, to ensure that they maintain their effectiveness.

## Social Responsibility
The aim of this policy is to comply with all the requirements of the SA8000 standard for the protection of workers, in particular, expressly prohibiting child labor, forced labor, human trafficking, and any form of discrimination. Promote the right to association and collective bargaining, respecting the disciplinary procedures provided for by the applied CCNL and guaranteeing remuneration and working hours that comply with current regulations.

Agilelab's Management commits to pursuing the stated objectives using the tools provided by the Integrated Management System and specifically:
- Carry out and review the context factors and the needs of the interested parties by identifying and evaluating the system's risks and opportunities.
- Ensure that the Integrated Policy is disseminated, understood, and implemented at all company levels, by all those who work on its behalf, and made available to all interested parties.
- Use technologies aimed at the continuous improvement of product quality, environmental protection, and safety, and adopt the best techniques available on the market to improve the performance of the works.
- Communicate the policy and objectives of the integrated management system to stakeholders.
- Communicate company performance, through the annual issuance of the Sustainability Report, to all stakeholders, promoting their involvement.
- Train and raise awareness of all staff, especially company managers, in the implementation of the Integrated Management System, so that the guidelines of this policy and the concrete objectives in terms of quality, environment, safety, and social responsibility are understood and accepted by all staff at various levels.
- Periodically evaluate, through internal audits, the compliance of the Integrated Management System with reference standards, its policy, and what has been planned and programmed; in particular, by verifying the achievement of the set objectives through periodic management reviews.
- Continuously improve the Integrated Management System based on the results of the reviews.
- Implement the involvement of all workers in company life through the introduction of procedures and tools to promote dialogue and improve the company climate.
- Implement the continuous improvement of communication, information, and stakeholder involvement channels.
- Promote supplier involvement and implement awareness/monitoring systems to verify compliance with worker safety and social responsibility requirements.

Any complaint regarding aspects related to the application of SA8000 by Agilelab can be forwarded directly to the email <DPO@agilelab.it>. For references to SAAS SAI and TUV, the contacts are:

### SAI
New York, USA
Phone: 212-684-1414
Fax: 212-683-8867
Email: <info@sa-intl.org>
### SAAS
Social Accountability Accreditation Services
tel: (212) 391-2106 fax: (212) 684-1515
<saas@saasaccreditation.org>
### TÜV Italia SRL
tel: +39 02 24130 1
Fax: +39 02 24130 399
<tuv.ms@tuvsud.com>

Update: 26/03/2023

