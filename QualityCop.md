# Quality Cop

Quality Cop is a role instituted by the Software Factory circle.
 
The role is focused on two main accountabilities: 
- Monitoring the overall project quality
- Enforcing the development guideline defined in [Software Guidelines](SoftwareDevelopmentGuidelines.md).

## Monitoring

Periodically each QualityCop should report a list of metrics to track the project's quality status.

Metrics include information about the project implementation details such as:
- test: unit tests, integration tests, coverage percentages
- documentation: readme files, Scaladoc/Javadoc, external documents
- style and formatting: enforcing of style rules, code formatting
- CI/CD: if present, which steps are defined
- issue life-cycle management: external/internal, issue-MR cycle, mode adopted
- versioning structure: git infrastructure, merge/rebase

Metrics are collected into `SoftwareFactory/Quality/Assessments/assessments.xlsx` file.
Refer to the FAQ tab in the metrics file for further information about the quality assessment document. 

## Enforcement

Starting from Quality Report metrics, Quality Cop should identify which project's aspects need to be improved to increase the overall project quality.

Whenever the project needs improvements due to the lack of software guidelines engagement or poor quality, the following process steps should be followed:

1. Define the set of tasks needed to improve the project's quality
2. Discuss with the project's leader about the tasks' feasibility
3. Add all feasible tasks into the project's backlog

## How to join Quality Cops

Each Software project in AgileLab should have at least a Quality Cop.

The underlying idea is to spread the quality culture to all development teams and raise the average quality for all projects.

The Quality Cop is typically a team member responsible for highlighting possible issues in project implementation.

To join Quality Cops:
- Check on Holaspirit the role Quality Cop isn't already filled for your project.
- Send a message to Software Factory Lead Link and ask to be added as Quality Cop.
