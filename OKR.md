By adopting the OKR methodology, Agile Lab goes beyond the well-known limits of most organizations which are based on a _command-and-control_ model.

As a matter of fact, most of the traditional organizations deal with
slow decision-making processes, difficulties in keeping a high level of innovation, and a lack of employees' commitment and ownership.

What are OKRs?
# OKR
**OKR (Objective and Key Result)** is a goal-setting framework conceived to spread the company's strategy among employees and to support the efficiency in the execution of tasks with a transparent and collaborative approach.

From a historical point of view, the OKR method is generally attributed to Andrew Grove, who introduced it to Intel during his tenure and later to John Doerr at Google.

As the name suggests, an OKR is made of an _objective_ and some _key results_

- the **objective** describes the company priorities in the short term, consistently with the company purpose and the long-term vision.
  This is the _outcome_ of the process since it explains "what" we want to achieve.

- **key results** are a list of results that must be numeric, measurable, and linked to the objective. They show how near the company is to the objective.
  They are the _output_ of the process since they explain "how" the goal will be achieved.

As a rule of thumb, a company should have no more than 2 OKRs per cycle. This will lead to the choice of real priorities. Ideally, one objective could be more aspirational and the other one more technical.

The cycle should last from 3 to 6 months.

A good objective should be:
- feasible in one cycle
- shared among the organization members
- inspirational and not numeric
- clearly in line with the company business
- challenging, but realistic
- under the team's control
- in the form "_verb_ + _what_ do you want to reach + _why_ + the application field"

Good key results should:
- be measurable on a weekly basis
- not be binary
- not exceed the number of 3-4 per objective
- clearly show how close the goal is
- be limited to the only ones useful to reach the target
- represents 100% of the objective
- not be influenced by external actors and dependencies
- in the form "_verb_ + _what_ will be measured + the _extension_ of the result [owner]"

This is an example of a good OKR:
- OBJ: Increase the employees' digital skills to sustain the ongoing digital transformation process of the company and to reduce costs by the end of June
- KR1: Write a gap-analysis document with x pages comparing the internal skills to the ones of the reference market [owner: Training]
- KR2: Reduce digital experts' turnover from x% to y% [owner: Peopleops]
- KR3: Hire x digital experts [owner: HR]
- KRn: ...

## Cascading OKR

One of the most powerful aspects of the OKR framework is the ability to align each employee's effort and decision, from the front line through the most operational areas to the company strategy.

This is possible because **a key result of the higher level becomes an objective of the lower level**. 
These are called **cascading** OKR



<img src="images/cascading_okrs.jpg" alt="Cascading OKR"  />



**Company OKRs** are at the top level and **Team OKRs** are at the lower level.

Every company should choose the correct number of cascading levels according to their cultural and organizational patterns: 
some organizations use more OKR levels, others can choose to not use cascading OKRs at all.

## Types of OKRs

Many organizations (e.g. Google) divide two types of OKRs: **committed** and **aspirational**. 
They have different purposes and they can be designed, interpreted, and acted upon in different ways.

- **Committed**
  
  These are goals that have to be reached in order to achieve the objective.
  
  Everyone at the organization and at the team level recognise that they must be met in full. In general, committed OKRs are to be achieved within a set time frame.
  
  For example, if the objective states "To become partner of a vendor" and the vendor requires at least a minimum number of certifications, the "achieve 10 certifications" is a committed OKR because it must be totally achieved.
  
- **Aspirational**

  On the other hand, aspirational OKRs reflect bigger-picture, higher-risk and more future-tilting ideas. They show how we’d like the world to look. They are sometimes called *10x goals* or *moonshots*.

  Regarding aspirational OKRs, Larry Page said "If you set a crazy, ambitious goal and miss it, you’ll still achieve something remarkable.”

  With aspirational OKRs, there is no clear path to  get there—and no real knowledge of what resources will be required.

  The expected success score of an aspirational OKR is 70%.


The relative weighting of these two types of OKRs is a cultural question.

# OKR process in Agile Lab

There is no ‘one size fits all’ OKR process that works for each company.
Therefore, Agile Lab adopted the framework crafting its own process and improving it cycle after cycle.

The {% role %}Agile Lab/OKR Champion{% endrole %} is in charge of the process milestones, timelines and outcomes.

This is a 10.000 foot-view of the OKR cycle timeline:
- The CTO and the CEO define the short-term strategy
- Objective(s) disclosure
- Definition of KRs (OKR weeks)
- Execution and monitoring with weekly meetings
- Review
- Retrospective 

## Objective(s) disclosure
One of the superpowers of the OKR framework is the alignment of all employees with the company strategy. As a matter of fact, every OKR cycle starts with the disclosure of objectives and their underlying reasons.

The OKR Champion collects employees' willingness to participate in the core parts of the process and communicates the objectives to the whole company along with the OKR weeks period.
OKR weeks meetings are planned in the OKR group calendar with adequate notice.

## OKR weeks
During the **OKR weeks** most employees can contribute to the company strategy by crafting key results in a collaborative way.
The OKR weeks start with a kick-off meeting during which objectives are recalled and explained throrougly by Agile Lab CEO and CTO.

Please note that the OKR weeks timeline can be slightly different from one cycle to another, because the OKR Champion can decide to use or not cascading OKRs to keep the process as simple as possible.

### Scanario 1: OKR Weeks with cascading OKRs
This is a sample timeline of a typical OKR Week with a single objective, when cascading OKRs are used: 

<img src="images/okr_weeks_timeline_with_cascading.png" alt="OKR week timeline with cascading" style="zoom: 100%;" />

|Day   | Title  | Activities | Goal of the day  |Attendees|
|---|---|:--|---|---|
|1|OKR kick-off and groups formation| The OKR Champion introduces the new OKR cycle, recalling the main steps in the OKR process, the OKR weeks timeline and the relevant training material about the framework<br/><br />CEO and CTO present the short-term strategy and the underlying reasons<br /><br />During the meeting, or by the end of the same day, the OKR Champion defines the OKR Team composition and divides it in some groups with (if possible) homogeneous functional representativeness and expertise. Moreover, a person is chosen by the OKR Champion as group secretary in charge of creating a proper communication channel with the rest of his/her group (e.g. Teams channel, meetings)<br /><br />Each work group is asked to produce ideas about company key results setting brainstorming meetings in the next few days. | Comprehension and sharing of:<br>- OKR framework fundamentals<br>- company's short-term strategy and objectives<br>- OKR Weeks goal process<br>-OKR Team and groups defined | Agilers |
|2|Proposals for company KRs|Before working on KRs, each group be sure to have have a crystal-clear understanding of the objectives. If not, it is important to seek clarification from CEO or CTO<br>Afterwards, each OKR Team group works autonomously to create proposal for company key results and writing them down in a board shared by the OKR Champion<br /><br />Groups can also invite other colleagues (even not belonging to OKR Team) if an external point of view is needed (e.g: if the team needs the certification coach's opinion about a proposal, (s)he can be invited as *external* expert) |  |OKR Team groups work autonomously for 2 days|
||| | ||
|3|...|...|- Group spokespersons chosen for each group<br>- Proposal for company KRs written in a common board|...|
|4|Ideas gathering for company KRs| A spokesperson chosen within each group presents KR proposals created by his/her group<br /><br />Company KR proposals are evaluated, filtered and grouped in a collaborative way to create company key results | First draft for company KR |OKR Champion + Group spokespersons + Main stakeholders|
|5|Company KRs tuning and owners assignment| Company KR proposals are fine-tuned and converted into well-defined KRs<br /><br />Each company KR is assigned to one or more owners<br><br /> | Company KRs defined according to OKR rules<br />At this point, company KRs can still contain approximate numbers that will be refined later on<br /><br />Temporary company KR owner(s) assignment |OKR Champion + Group spokespersons + Main stakeholders|
| 6 |Proposals for team KRs|Because of the "cascading" mechanism, company KRs become team objectives<br />At this point, the OKR Team is dismissed and each company KR owner is asked to create a _focus_ group made up of people who subject matter expert on the specific topic of the key result.<br />The focus group's goal is to create KRs proposals for the team objective (linked to the company KR)<br /><br /> | |Focus groups work autonomously for 2 days|
| 7 |...| ... | Proposal for team KRs written in a common board |...|
|8|Ideas gathering ideas for team KRs|Each owner presents his/her group's ideas for team KRs<br><br />Team KR proposals are evaluated and filtered to create a subset of well-defined team KRs||OKR Champion + OKR owners + main stakeholders|
|9|Team KRs tuning e feasibility check| Team KR proposals are fine-tuned and converted into well-defined KRs<br /><br />If at this point some company key result is still assigned to more than one owner, the OKR Champion selects a final owner according to the functional affinity <br />Each owner should accept his/her KR and conduct a feasibility check on it |Team KR defined according to OKR rules<br /><br />Definitive company KR owner(s) assignment|OKR Champion + OKR owners + main stakeholders|
|10| OKRs owner buy-in and freeze | Company OKR owners propose definitive numbers for their KRs while bearing in mind the difference between committed and aspirational OKRs<br /><br />KR owners refines their KRs, ensuring that all the properties for a good-KR are satisfied and that it will be understandable along the cycle. No misunderstandings should happen during the monitoring and execution phase. | OKRs defined and reported in Holaspirit |OKR Champion + OKR owners + main stakeholders|

### Scenario 2: OKR Weeks without cascading OKRs
When we want to achieve more than one objective, it is often better to avoid cascading OKRs to streamline the process.
In this case, everything related to team-level KRs is not explored in depth and when Company KRs are created in the first week, the OKR team starts to work on the second objective.

Basically, meetings from day 6 to 9 are replaced by a new round (repeating the timeline from day 2 to 5), except for the fact that the OKR team works on the second objective (if a second objective exists) 

This is a sample timeline of a typical OKR Week, when cascading OKRs are **not** used and two objectives are there:

<img src="images/okr_weeks_timeline_no_cascading.png" alt="OKR week timeline without cascading" style="zoom: 100%;" />

### OKR Team composition
The OKR Champion is in charge of creating the OKR Team and its groups.

Even if no exact formula exists to create a proper team, still there is a pattern that can be used to have a skilled and engaged group of people working together with an homogeneous skillset in each group.

For efficiency reasons, the OKR Team consists of a limited number of people so that brainstorm meetings can take place effectively.
This constraint given, members are chosen either for their _role_ or their _willingness_ to participate in the OKR weeks (a survey is often used for this purpose).

As a rule of thumb, this order of precedence is considered:
- people leading or working in Circles relevant to the objective
- people who express a clear willingness to participate
- people who express a not-so-clear willingness to participate
- people leading or working in Circles non-relevan to the objective

### End of the OKR weeks

In any case (with or without cascading OKRs), the last meeting of the OKR week performs a feasibility check of KRs and the KR owner are asked to buy-in their KRs.

After the OKR assignment, each KR owner acts as a project manager.

For example, (s)he:
- creates a project team undertaking all tasks related to his/her OKR ensuring that the people with the right skills are involved
- identifies dependencies with external teams and plan tasks
- assigns an owner to each team OKR
- tracks the progress of all team OKRs linked to his/her company OKR
- asks the OKR Champion the needed budget
- tracks tasks progress on Holaspirit
- attends status update meeting communicating his/her OKR progress and issues with the proper level of detail
  

## Execution and monitoring

Efficiency in the execution stage is one of the framework pillars. Therefore, after the OKR week we start tracking the progress of the OKRs projects in weekly status meetings.

These are vigorous conversations where OKR owners both explain the updates to be tracked and highlight roadblocks. That will keep everyone focused on the top-priority tasks.

During these meetings, each OKR owner gives thorough details of:

- **Progress**: the percentage of OKR completion
- **Confidence level**: to be labelled as "Healthy", "Need attention" or "Unhealthy".
  The owner can talk about roadblocks or issues he/she is encountering.
- **Week priorities**: the list of priorities he/she will be focused on in the following week to meet the objective. The goal is to inform people about the main issues that they should be aware of.
- **4 Weeks events**: the list of the OKR-affecting events that could happen in the following 4 weeks. This is useful to keep the whole team on the same page.

## Review and retrospective
At the end of the cycle, the OKR Champion organizes a review, and a retrospective meeting to understand what went wrong and how to improve the process.

An objective is achieved if the raw goal score is at least 70 percent. This numeric value is used to state if the object is reached in a "yes or no" fashion.
A good retrospective cannot be based only on a numeric value, since it might hide a strong effort and extenuating circumstances, or weak management and procrastination of the activities till the last days of the cycle.

A useful tool the OKR Champion can use to drive the retrospective process is the scoring and assessment variations tables. Here is how an OKR can be filled in in the retrospective (in real case you'll have one row for each OKR, we report the same OKR just to show different scenarios as an example).

| **OKR**                         | **Progress** | **Score** | **Self-assessment**                                                                                                                                                            |
|-----------------------------|----------|-------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Bring in ten new customers. | 70%      | 0.9   | Due to a slump in the market, the OKR was significantly tougher to achieve than I’d thought. Our seven new customers represented an exceptionally good effort and outcome. |
| Bring in ten new customers. | 100%     | 0.7   | When I reached the objective only eight weeks into the cycle, I realized I’d set the OKR too low.                                                                        |
| Bring in ten new customers. | 80%      | 0.6   | While I signed eight new customers, it was more luck than hard work. One customer brought in five others behind her.                                                       |
| Bring in ten new customers. | 90%      | 0.5   | Though I managed to land nine new customers, I discovered that seven would bring in little revenue.                                                       |

Columns meaning:
- **OKR**: the name of the OKR
- **Progress**: the objective value that state the percentage of completion of an OKR. If the progress is >= 70%, we succeeded in the work. Otherwise, we failed it.
- **Self-assessment score**: the self-evaluation score that the OKR owner gives to himself/herself and to his/her team. It is a subjective score that can summarize several aspects: the team's commitment along the cycle, how significant that team's result was to achieve the goal, etc. During the discussion in the retrospective session, this value can be confirmed or changed.
- **Self-assessment comment**: verbose explanation of the *Self-assessment score* column. "Why did we give a score of 0.9 despite the OKR failed?", "Why did we give a score of 0.5 despite the OKR succeeded?", "Was our result really relevant for the objective?", "Was the team's engagement consistent and well planned throughout the cycle?"
  These are some of the topics we want to clarify here.

If the OKR is achieved or not is still stated only from the *progress* column.

Keep in mind the only purpose of the table is to add contextual information to the final review. A `60% okr` can bring more value to the company compared to a `90% okr` just because the second result was set too low at the beginning of the cycle. This value brought to the company should be explicit. In this case, you'll have

| OKR | Progress | Score | Self-assessment |
| ------ | ------ | ------ | ------ |
| "60% okr name" | 60% | 0.9 | The OKR was not reached but the team worked very well despite external blockers  |
| "90% okr name" | 90% | 0.5 | The team didn't work as a real team since the objective was too easy to reach. Spare activities all in the last two weeks of the okr allow us to reach the objective without issues |


# How to track hours spent on OKRs

OKR is a tool that helps employees to:
- find out what are the most important results to achieve strategic company goals
- achieve these goals in an efficient way

To achieve results, each department/Circle must undertake one or more project defined in the OKR cycle, but from a financial point of view these projects are linked to the department/Circle.

For this reason:

- hours spent in OKR meetings (kick-offs, OKR weeks brainstormings, status update) must be tracked in the "YYYY Agile - GCC - OKR" issue
- hours spent by a department in OKR-related initiatives must be tracked in a proper department-specific issue

------

# Credits

- Measure what matters [John Doerr]
- Radical Focus [Christina Wodtke]
- OKR Performance [Francesco Frugiuele, Matteo Sola]
- https://www.whatmatters.com/
