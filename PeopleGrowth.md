# People Growth

Caring about people growth is one of the key elements of Agile Lab to be successful.

Making people advance in their career path is a valuable iteration of everyone's professional life and, it contributes to our growth as humans as well.
There are many ways to make people grow and here we will discuss how Agile Lab cares about _agilers_.

Ultimately, the responsibility for one's career path lies with the individual team member, not the coach. The coach can facilitate, assist, and advocate for it, but each person is ultimately responsible for their own growth. You must take ownership of your career path; promotions will not materialize out of thin air or solely because of your coach's influence.


At Agile Lab we approach people growth in many ways:
- **Tutorship**: teaching people new things driving them through a personal path; usually short-term and laser focused on specific topics;
- **Mentorship**: mentors lead new _agilers_ by examples. Mentors are expected to proactively teach things, explain concepts, help resolving issues. 
  In turn, mentees are expected to be eager to learn, proactive asking questions, experimenting, failing to understand and improve.
- **Coaching**: a coach provides direct hints, recommendations, suggestions, feedback. It does not take direct actions on behalf of a coachee.  

## Tutorship

People growth starts since the first day at Agile Lab.
Everyone onboarding our company receives an initial tutoring by our [Buddies](Buddies.md).
At this stage we introduce new employees our culture, practices, tools and all the necessary means to be autonomous working on a daily basis.
This is a fundamental part of people growth since this gives everyone an initial taste of our identity and values.

## Mentorship
A mentor follows people growth for a mid-long overlap of their working life. 
Mentorship is more directive since mentors know which results to expect from their mentees.
Usually, mentorship is provided by peer programming, shadowing or any other activities done side-by-side with the mentee.
Mentorship tends to be more directive to address specific problems tries to tackle software design issues, troubleshooting, etc.).
Anyway, our mentorship style is strongly based on the proactivity of our mentees.
Thus, we expect our _agilers_ to work at their best and tackle their own issues with professional attitude.

## Coaching
A coach stimulates people brain, makes her/his colleagues growth giving hints, recommendations, providing feedbacks, and challenging their decisions.
Of course, our mantra is "leading by example". Anyway, coaching should not invade people's space in order to let them learn from their own experiences.
Instead, coaching means moving the right human and professional levers by sharing thoughts, reasoning together and making the right questions. 
For instance, a brilliant engineer moving to her/his first project leadership experience may need some initial shadowing (closer to mentorship than coaching).
As soon as she/he gets the right grip to move autonomously, coaching comes into play.

# Leadership
How does leadership matter with people growth?

At Agile Lab, we give [leadership](LeadershipAndCoaching.md) very much attention.

**Leadership is the ability to influence opinions, decisions, people.**

There are good leaders and bad leaders. We aim to grow wonderful leaders and this requires an important dose of people growth effort.

We excercise our leadership to provide our customers with elite data engineering principles, practices, services, and products.
We want our professionals being able to manage complex initiatives, projects and situations while feeling safe and confident about their actions taken with awareness about risks and benefits.
Our leaders work to growth new leaders and build a community of professionals.
Building leaders means nurturing self-actualization based on a solid methodological, technical, and organizational background.
Our leaders convey tutorship, mentorship and coaching at many levels.

Whether behaving as mentors or coaches is inherently situational.
It depends on seniority of trainers and trainees, type of activities and accountabilities to make people feel safe with.  

**For this reason, you may find in the handbook the terms mentor, coach and leader used with the same intention: leaders caring about people growth.**

![alt text](images/coaching-vs-mentorship.png)

## Lead Links
A lead link is in charge of the management of a piece of the organization following our self-management principles as stated in Agile Lab's [constitution](./Constitution.md).
Lead links should mentor and coach their circle members with particular attention to [team leaders](#team-leaders) since they are responsible for a whole team and may need more support.

## Team Leaders
Our activities are often organized by teams. Every team is assigned to a [Team Leader](LeadershipAndCoaching.md) responsible for team members' growth.
A team is always an important occasion to learn for everyone. Team leaders can learn how to mentor/coach our engineers and engineers can learn by examples from our team leaders.
Usually this is a mid-long term path where _agilers_ have a constant occasion to improve and receive feedback.
A team leader helps her/his colleagues to ramp up their [career path](EngineeringLadder.md) through the achievement of goals within and outside the scope of their own projects according to company's objectives and [purpose](Company.md).
This is very important to create alignment between personal/professional expectations and company's objectives.

## Mentorship in team working
The team leader is not the only mentor within a team.
In a team, people having more experience care about younger _agilers_ to make a team stronger and more effective.
An effective team makes everyone improve. People eager to improve makes a team stronger.

## Community Of Practices
At Agile Lab we have established a [Community Of Practices](./CommunityOfPractice.md) for [Leadership & Coaching](./LeadershipAndCoaching.md) where to discuss how to improve our practices for people growth.


# Owning your career

Own it !
Why ? Because nobody cares about your careeer as much as you do.

While coaches are eager to assist with team members' careers, they are not the best people to manage them. The most suitable person is the individual themselves. Those who take initiative, set goals, track progress, and continuously improve are best positioned for growth.
Take ownership of your career if you aim for success. Don't just wait for a coach to intervene. There are plenty of ways to own your career, such as informing your coach and peers about your interests, showcasing your work, and creating opportunities for feedback.

## Get Stuff done

Complete the tasks assigned to you and deliver high-quality results. Over-deliver when possible and maintain consistency over the long term. More importantly, ensure people are aware of your accomplishments; don't assume your coach and peers will automatically know. Communicate specifically about impactful achievements to your coach and team.

Maintain a Work Log and make sure it's visible to your coach.

## Ask for feedback

One of the best ways to grow professionally is through feedback from colleagues. To get meaningful feedback from peers and your coach, ask about specific situations or tasks (e.g., "I recently worked on this task, what do you think about my coding style?" or "How do you think the strategy meeting went? Were my contributions insightful?"). Avoid asking for general and vague feedback (e.g., "How am I doing?") as it often leads to non-specific responses like "nothing, everything is good," which isn't helpful for your growth.

## Help your coach to help you

* Have regular one-on-one meetings; request them if your coach forgets to schedule.
* Prepare the agenda and ensure your coach understands your priorities and goals.
* Seek specific feedback.
* Don't assume your coach is aware of all your work. They likely have a lot on their plate too, so help them focus on your context.
* Establish mutual trust. Be open, honest, and transparent. Share the unvarnished truth without fear of judgment.



# Career

Despite the inspiring stories on LinkedIn about people advancing from junior to senior positions in three years, such rapid progression is unlikely at Agile Lab, and we believe you shouldn't be overly concerned with these examples.

Avoid letting promotions, titles, and salary define you and your self-worth. It's easy to fall into the trap of comparing yourself to those at higher levels or in different companies, leading to frustration. Focus instead on your own path. Assess the pros and cons of your current situation and strive to make incremental improvements based on what matters for you; there's no need to rush. Life and career are "infinite games" where there are no definitive winners or losers, only those who are "ahead" or "behind." However, it's important to remember that the definitions of "ahead" and "behind" are subjective and based on your own perspectives and expectations.

Be wary of biases when comparing yourself to others, based on your personal evaluation criteria. Always strive to understand the criteria used by the company.

It's easy for a senior engineer to look at a staff engineer and believe they are performing certain tasks better. Resist this urge to compare. Promotions and levels are not solely based on specific skills but also on creating consistent impact for the company when opportunities arise. Opportunities within a company are not always distributed evenly; it's important to accept this.


## Value creation

If you're seeking more opportunities and want to create an impact, ask yourself: What does my coach value? What is important for Agile Lab? Set aside your personal priorities and focus on delivering results that are significant to the company.

Consider what Agile Lab values more: 
Consistently performing and solving concrete problems in challenging projects, or exploring new technical languages? 
Does a coach prefer team members who autonomously solve customer problems, or those who require their limited time to resolve customer issues? 
Does Agile Lab value individuals who can create thought leadership in the market and directly impact revenues, or those who focus solely on technical implementations and internal matters? 
Is it more beneficial for Agile Lab to have team members capable of building long-lasting relationships with customers, or those who prefer to switch projects annually? 
Does Agile Lab prefer individuals who embrace its values and beliefs, or those who oppose them?

Those who align with the company's and coach's priorities and contribute to the company's value generation will have more opportunities to express their potential and then advancein their path.

Compensation and promotions are exchanges of value between the company and the employee. The higher the value delivered to Agile Lab, the greater the compensation and advancement. However, what constitutes "value" is defined by the company, not the employee. The value all the employees generate is based on the Agile Lab values, goals and market context.



## Investments pay off later

Nowadays, people often want rapid success, but it's essential to remember that your career might span over 40 years. If you become a Staff Engineer in three years, what progression can you expect in the remaining 37 years? It's realistic to acknowledge that most people won't reach Level 4+ positions. Many tech companies have a "terminal level," meaning "get promoted or get fired." We don't subscribe to this philosophy at Agile Lab, but it indicates that also in other companies not everyone can advance to higher levels. This is acceptable, and we should approach it with serenity and transparency.

Treat your career as a journey to discover your full potential. Stay curious and don't rush. Sometimes, moving sideways—whether into a new tech stack, a new role, exploring leadership, or accepting new challenges might seem to slow you down in the short term, but it can accelerate your growth in the future. This approach allows you to accumulate more experiences, perspectives, and structure.

