# Milano

- Address: [Bastioni di Porta Nuova, 21, 20121 Milano MI](https://goo.gl/maps/rpuWzVjZV8LKjy8c9)
  + [Spaces Porta Nuova](https://www.spacesworks.com/it/milan-it/porta-nuova/)
- Parking:
  + Inside parking not available right now.
  + [Underground Car Parking XXV Aprile](https://www.apcoa.it/en/parking-in/milano/xxv-aprile/).
- Public transportation in Milan is handled by [ATM](https://www.atm.it/en/Pages/default.aspx):
  + By metro: 
    * *Garibaldi* stop, then five minutes by walking (best option if you're coming from the [*Milano Porta Garibaldi*](https://www.trenord.it/en/routes-and-timetables/journey/station/?no_cache=1&mir-code=S01645&cHash=f02d02f98fa66fbe464b7171d7c5f1b5) train station).
    * *Moscova* stop, then five minutes by walking.
  + It is possible to purchase ATM tickets for the metro downloading the [*ATM Milano Official App*](https://www.atm.it/en/ViaggiaConNoi/Pages/ATMMobile.aspx).
- Seats:
  + Total: 26 (seats are not named).
  + A *badge* is required to enter. The badges can be retired at the reception (Monday - Friday, 8:30 a.m. - 7 p.m.)
- Office spaces: 
  + _Operative area_:
    * Fourth floor:
      - Agile Lab offices [410,414]:
        1. One large office space with 10 seats (410, 411, 412).
        2. One small office space with 3 seats (413, 414).
    * Ground and Underground floors:
      - Coworking open-space with 13 seats for Agile Lab.
      - Phone boots where you can make private calls (no booking and no time limit).
      - Meeting rooms.
  + _Recreative area_:
    * Ground floor:
      - Coffee/snack bar.
      - Relax area.
    * Fifth and Top floors:
      - Panoramic terraces accessible for smoking, taking a break, eating...
