# Knowledge Challenge Pills

*Knowledge Challenge Pills* is a new initiative within the Banking Business Unit that aims to foster knowledge sharing, skill development, and collaboration among team members. This program revolves around the periodic distribution of quizzes and challenges related to various skillsets. Questions spans across [4 main tracks](#challenge-tracks): domain knowledge (banking), architecture and design, tech and communication.


## Objectives

The primary goals of the Knowledge Challenge Pills initiative are as follows:

1. **Sharing Knowledge**: The initiative focuses on promoting knowledge sharing within the Banking Business Unit. Through challenges, team members can share their experiences, insights, and solutions related to real-world scenarios encountered in their roles. Challenges will be categorized based on roles and levels to facilitate the learning experience.

2. **Interview Question Repository**: By collectively curating and validating challenging questions, the initiative aims also at building a validated repository of interview questions with the goal of minimizing personal bias during interviews and ensure a fair assessment of candidates' abilities.


## Challenge Tracks

The challenges will be organized into the following tracks:

1. **Domain Knowledge (Banking)**: Questions in this track will focus on general knowledge of the banking sector. Questions may cover scenarios related to financial regulations, industry practices, and banking operations.

2. **Architecture and Design**: Challenges under this track will delve into evolving architectural solutions to fit new requirements, data modeling, and low-level design (LLD) considerations.

3. **Tech**: In this track, participants will tackle technical challenges, such as bug fixing, coding problems, and completing specific functions.

4. **Communication**: Challenges within this track will address situations from a management perspective, including handling difficult communication scenarios and effort estimation.

## Challenge Format

The challenges will be distributed using the "Banking/Knowledge challenge pills" Microsoft Teams channel. Each challenge will follow the specified format:

```
Title [Estimated Resolution Time] 
#Track {Arch/Design, Communication, Tech, Domain Knowledge}

Description (snippet, images, and files links)
```

The initiative started with an initial goal of 2 challenges per month.

## Participation and Contribution

All members of the Banking Business Unit are encouraged to actively participate in the Knowledge Challenge Pills initiative. Contributions can be made by submitting challenges to the selected individuals responsible for managing the project via email. By sharing your insights and experiences, anyone can contribute to the growth and development of the entire team.

## Future Expansion

While the Knowledge Challenge Pills initiative is currently being tested and validated within the Banking Business Unit, the long-term vision is to extend this practice across multiple business units within the company. Scaling this initiative company-wide will create a culture of continuous learning and collaboration, benefiting all employees and the organization as a whole.
