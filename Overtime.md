# Overtime

- [Overtime isn't encouraged](#overtime-isnt-encouraged)
- [Self-manage your goals, time and energy](#self-manage-your-goals-time-and-energy)
- [Work with your manager in unusually challenging situations](#work-with-your-manager-in-unusually-challenging-situations)

## Overtime isn't encouraged
In Agile Lab **we don't** want to **encourage to work overtime** because in the long period can lead to burnout, and we care about our people. 
Our working hours are flexible, and people are strongly encouraged to balance work and personal life priorities.


## Self-manage your goals, time and energy
Each individual must understand when to put more effort into the job and when it is possible to regain space in private life. We want people to be free to organize their activities at their best without any constriction but always acting with self-responsibility.

As a matter of **personal choice**, you may want to focus more on the job and work **some extra hours** to hit a goal for a week. After such an intensive week, we strongly recommended to slow down and recover during the following days. 

To **slow down, recover and claim your time back**, you don't need to notify anybody. If you have worked some extra hours, you can spend them whenever you want.

On average, the hour count on your timesheet *must be* balanced.

## Work with your manager in unusually challenging situations
There may still unusually challenging times when it is difficult to keep such balance. In that case, you have to **notify your manager immediately** and start to record extra hours as `SA-Straordinari ad accumulo` in your timesheet.

Your manager will periodically review extra hours recorded as `SA-Straordinari ad accumulo` in your timesheet and will try to **find a way to reduce the load and give you back those hours**. At the end of the semester, unbalanced extra hours will be paid.

This mechanism is devised to encourage people not to work extra hours.
