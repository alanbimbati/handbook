# DVR - Documento di Valutazione dei Rischi (Risk Assessment Document)

Risk Assessment Document is the document that the Employer, according to Legislative Decree 81/08, the so-called Consolidated Occupational Safety Act(Testo Unico Sicurezza sul Lavoro), must prepare to assess all the risks to which workers working in his company are subject.

Please click [here](https://agilelab.sharepoint.com/:b:/s/bigdata/EUEujg915upCjwuDe1--PqUBYeSIYbIblSfZBZw14ArBKg?e=8mXLHm) to read the relevant document.