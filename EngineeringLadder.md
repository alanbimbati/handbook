The Engineering Ladder is our way to provide people a professional growth path. 
What is needed to advance to the next level and get a promotion is well explained and explicit (transparency).

Our Path is composed of seven levels. The first four are the same for everyone, while the last three are different for tech and manager tracks, as shown in the below image.

![Ladder](images/ladder/ladder.png)

![Rookie](images/ladder/rookie.png)
![Eng1](images/ladder/eng1.png)
![Eng2](images/ladder/eng2.png)
![Eng3](images/ladder/eng3.png)
![Eng4](images/ladder/eng4.png)
![EngManager](images/ladder/engmanager.png)
![Staff1](images/ladder/staff1.png)
![EngDirector](images/ladder/EngDirector.png)
![Staff2](images/ladder/staff2.png)
![VPEng](images/ladder/vpeng.png)
![CTO](images/ladder/cto.png)




The Engineering ladder is not related to self-management roles. Self-management roles are needed by the organization, while each individual requires ladder levels to grow as a professional. It helps to develop skills across four main directions:
* Dex - Technical skills
* Str - Get Stuff Done
* Wis - Impact
* Cha - Communication & Leadership

The extended document is located in BigData Sharepoint / Career Ladder and it will be updated and releases every six months by the `role:Engine/COO`

This ladder was built taking inspiration mainly from the "rent the runaway" ladder and many others. Thanks


### Rules

* Expectations at each level are cumulative
* Managers have the same expectations as individual contributors to the extent that they are doing individual work, in addition to manager-specific expectations
*  Dex and Str ( the «adder» attributes) are the primary drivers of promotions at lower levels. At higher levels, the 
importance gradually shifts to Wis and Cha ( the «multiplier» attributes )
* Not everyone will progress in each column in lockstep. If you are Eng4 for Dex and Eng2 for Cha, you will not be stuck at Eng2, but you will likely not be promoted all the way to Eng4.  Higher levels require full compliance. 
* To advance to the next level, you need to consistently perform at that level for some time (6M – 1Y) and then 
needed by the organization
* To reach Engineer 3, a positive experience as Project/Team leader is helpful
* Pay attention to senior traits and quotas to advance to L3+​
* To reach Engineer 4 or above, an extensive experience as project/team leader is almost mandatory
* Each level has a compensation range associated. If you are stuck in a level, you can anyway move forward your compensation, but only up to the upper limit.
* Each level has a quota, calculated by the engine lead link based on the total number of people in the Engine department and their distribution across levels.
* Ladder levels are not applied to external consultants hired by other companies. Those hired remotely (E2 contracts) are instead included in the ladder mechanism. See [Contracts](Contracts) for details.
* For higher levels, it is not sufficient to demonstrate skill but is necessary to act consistently, generating a tangible impact. Very often is not possible to create such an impact without having the possibility to serve in specific roles. This is the motivation for level quotas because we don't want people doing activities for ladder climbing and not for the company's needs.
* In order to move from Engineer Rookie to Eng 1 the coach approval is sufficient.​
* Each level has a minimum working experience requirement ( years ). Minimum working experience is just a gate, but it is not an evaluation metric for the promotion. There are no upper limits in ladder levels. It is possible to remain forever in a level; no improvements, no promotion.
* Not all the people can be promoted to the higher levels.
* Bonus Pack is a one time bonus delivered at 31/03 of each year. The employee as of 31/03 must still be within the company and employed for more than one year. Bonus pack is only for employees ( E1 )




## Promotion philosophy

When is possible we approach things to bottom-up and with self-responsibility in mind. 
**Promotions are based on actual and consistent performances, not growth potential**. In order to be considered for a promotion the candidate should be executing at the next level prior to promotion.

The promotion is just a milestone in a long-term journey and it is the result of a growth process in partnership with the coach. Personal growth should be a frequently discussed item during 1-1 meetings, without creating the false expectation that because talking about growth then it means we are close to the next milestone. The coach should be super transparent in dealing with team member expectations, performances and improvements.

**Without consistent improvements and next ladder level performances, there will not be promotion**, no matter how long the team member is stuck in a ladder level. Fitting with the minimum working experience required for a specific ladder level is not an indication to be eligible for that level, **the past working years are not a driver for the promotion**.


### How to get promoted

* If you want to be promoted, you need to assess yourself and convince your coach that you are ready
* Ask your coach to assess with Engine lead link if there is quota into the next level and then present your candidacy sending an email to the upper lead link. The Engine lead link will prioritize in case of multiple application for a level that has no enough capacity. Within the candidacy you can attach your brag file or other kind of documentation that is demonstrating your target level.
* Your coach  will mentor you along the process, helping to build the brag file to support your pitch 
* Pitch will be done to a specific committee.
* if your mentor is not coaching you enough, please keep him/her accountable

By default, the coach is your project leader (or the lead link).
However, project leaders and lead links can delegate the coaching responsibilities to other team members they consider suitable for the role to ensure optimal coaching in the long term.

The coach has a huge responsibility in this process:
- if a candidate is not ready to apply for the next level and the coach does not detect it, it can cause a huge demotivation effect in case it is not promoted
- If a candidate is ready, but is not self-confident to apply for the promotion, could be demotivating as well because no-one is recognizing its value
- Coach must be super transparent and should always coach to reach the goal, but should not to sponsor a not ready candidate.

### When to get promoted

The candidate can apply to pitch at the beginning of each quarter within a two weeks window, and the candidacy must be presented at least one month before. 
For example if you want to pitch at the beginning of April (End Q1), the application must be done before the end of February.


### The brag file

Not all the people in the committee are aware of what the candidate is doing and how she/he is performing. The ladder is composed by a list of expectations that the candidate needs to fulfill. Most of them are soft or not easy to demonstrate in an interview, so the candidate should build a brag file with its major achievements and how these are fulfilling the ladder requirements. It is also a matter of transparency. 

The brag file should be a sharable document and it must be shared with the committee at least one week before the pitch, so they can prepare questions and can have a clear overview about it.
The document should be self-explanatory and should demonstrate that candidate is acting at the target level since 6-12 months with consistency. Put focus on generated impact and major achievements, the brag file should not be more than 5 pages/slides and should not require more than 40 hours of work ( you can use working hours to prepare your brag because it is a crucial part of your personal growth ). The side goal of the brag file is to raise awareness in the candidate, identify weak points and improvement paths.

Especially for higher levels, it is crucial to focus more on impact and outcomes than actions and self-referential items. For example, don't report "I created a new process for code reviews" without mentioning the positive impact of code review effectiveness and the level of adoption of this new practice within the company.
Because not all the ladder requirements can be documented the coach can certify for the candidate that he/she is compliant ( possibly with a cover letter ) or the committee can decide to ask questions about a specific topic to properly assess it. 

### Promotion Committee

We want to distribute the accountability and don't let a single person judge for a promotion, limiting the judgment bias and the candidate's frustration against a single person.

The following roles will compose the committee:
- The lead link of Consulting or Witboost plus two other components ( choosen by the lead link ) (at least Eng 4.)
- The candidate's coach (project leader/lead link)
- the lead link of the candidate's coach (upper lead link)
- 2 team members (or former team members) of the candidate, chosen by him/her. (if you are not in the position to pick up two team members, explain why and proceed anyway)

When a candidate want to schedule the pitch, the coach will ask the lead link of Consulting or Witboost to compose the committee and schedule the pitch.

### How to propose an application

The lead link of the candidate's coach:
- Calls for a meeting to compose the Promotion Committee sending an email to the Lead Link of The Elevator and two eligible other members at her/his choice
- Puts the candidate and her/his Coach in cc and attaches brag file and presentation
- Additionally, can anticipate the two members chosen by the candidate in cc

The lead link of the candidate's coach is also responsible to schedule the meeting taking into account everyone's availability.

The Coach answers with a cover letter for the candidate.

The period between the application and the meeting with the Promotion Committee will be used by the Committee members to review the material and provide further inputs before the presentation.


### The Pitch

The pitch is a meeting among peers, it is not an interview but the committee will ask questions about the brag file or will assess open points. The pitch and the brag file should focus only on expectations points that coach or the committee think must be proven.

After the pitch of the candidate, the committee will express an evaluation in the following way:
* Technical Skills: coach + people from the elevator circle
* Get Stuff Done: coach + team members
* Impact: coach + upper lead link
* Communication & Leadership: coach + upper lead link + team member

To get the promotion on each feature, you need to reach unanimity and the result will be given immediately after the pitch.

If you get not promoted, you will receive extended feedback about motivations and what you should focus on to get there.


### Compensation
The [compensation](SalaryReview.md#promotion) must always be decided by a person that is possibly two levels ( of the ladder) above the promotion's candidate's target level.


