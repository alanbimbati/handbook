# Hardware purchase and buyback

All employees are assigned to a physical facility (an office) even if they work remotely most of the time. If you need to buy something to work with to use it in the office, always ask the {% role %}People Care/Facility{% endrole %}.


The {% role %}People Care/Facility{% endrole %} is accountable only to buy assets and amenities related to the physical offices. All you need to work remotely should be included in the Welcome Pack.


Each employee, during onboarding, will receive a laptop, an additional screen and HW set (Welcome Pack).

Each component of the Welcam pack must be registered in the Census file with each employee completing the following form: https://forms.office.com/e/Bt59qfYdgR

Each time the employee replaces their laptop or any other HW, they must fill out the form again.

If you need to buy something that you don't ask for initially, and it is included in the Welcome Pack list, you can refer to {% role %}People Care/Welcome Pack & HW Replacement{% endrole %}.

If you need to replace something related to the Welcome Pack ask to {% role %}People Care/Welcome Pack & HW Replacement{% endrole %}. You can also refer to {% role %}People Care/Welcome Pack & HW Replacement{% endrole %} if you need to buy something that you didn't initially ask for in the Welcome Pack, but the request must stay in the welcome pack perimeter (e.g. if you need a particular keyboard, you can ask to buy a keyboard with a budget around the budget defined for the keyboard in the welcome pack; if you want an office chair to work from home, and that is not available as eligible purchase in the welcome pack, you can't ask for it).

After 3 years of use, an employee can ask the {% role %}People Care/Welcome Pack & HW Replacement{% endrole %} for the substitution of his/her laptop: the new laptop model and specifications are decided by Internal IT and updated frequently. When an employee asks for a laptop upgrade he/she can ask to buy the previous laptop for personal use.
This process of laptop redemption is regulated by the following rules:
*   The following rules apply **only for the laptop** assigned to each employee by the company.
*   After 3 years of usage (you can check this time in the census file) an employee can ask the Buyer to buy a new laptop. At that moment the employee can buy the laptop that is going to be substituted for personal use.
*   The employee can use the benefit and prize money to buy the old laptop (without the 30% discount since there is no VAT).
*   The price of the old laptop buyback is defined at the market price at the current date minus a 10% discount for the employee. A valuation of the market price will be performed by the employee by proposing an average of similar laptops on the used market (exact models or previous/following ones). The employee will send the list of links and evaluations to the Buyer that will check them before defining the buyback price.
*   Additional considerations on the price based on the state of the machine will be discussed case by case (e.g. you want to buy your old laptop even if it has some damaged components).
*   When the buyback is approved the employee must tell Internal IT that the laptop is no longer company property so they can apply GDPR un-enrollment policies.
