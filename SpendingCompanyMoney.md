

Spend company money like it is your own money. No, really.

# Office Equipment

The easiest way to buy something for the office or for your workstation is to ask `circle:People Care`

Specifically, the employee should contact:

- The {% role %}People Care/Welcome Pack & HW Replacement{% endrole %} for notebooks, additional screens or smartphones by writing an email.

    Policies defined in order to replace above mentioned hardware are:

    1. replacement may be requested after 3 years of hardware life;
    2. replacement can be made with hardware of the standard model defined by Internal IT and People Operation;
    3. replacement can be requested before the expiration of 3 years in case of failure.

- The Welcome Pack & HW Replacement for every HW components related to the Welcome pack.

    1. If you need to buy something that you don't ask for initially, and it is included in the Welcome Pack list, you can refer to Welcome Pack & HW Replacement.

    2. If you need to replace something related to the Welcome Pack ask to Welcome Pack & HW Replacement. You can also refer to Welcome Pack & HW Replacement if you need to buy something that you don't initially ask for in the Welcome Pack, but the request must stay in the welcome pack perimeter (e.g. if you need a particular keyboard, you can ask to buy a keyboard with a budget around the budget defined for the keyboard in the welcome pack; if you want an office chair to work from home, and that is not available as eligible buy in the welcome pack, you can't ask them).

- The {% role %}People Care/Facility Manager{% endrole %} of your office, writing to his/her email address, for every other daily equipment in the **physical offices**: Cables, Webcams, Headphones, Keyboard, Mouse/Trackpad, Laptop Stand.


Office equipment must be used for business only.


**Software**

We use Microsoft Office 365, typically the online office suite is enough, but if you need the desktop suite to do your job properly, just ask. 
If you need an additional software that requires a license do the following steps:
*   ask your colleagues if this software can solve also their needs and collect feedbacks
*   send an email to the {% role %}Finance/Purchasing Officer{% endrole %} explaining why it is important and how many people need it.  



**Business cards** 

Business cards, if needed, can be requested by emailing {% role %}People Care{% endrole %}



**Work-related books** 

In Agile lab we have office libraries.

If you want to propose a new physical book for a specific office,
- contact the {% role %}People Care/Facility{% endrole %} for the specific office and communicate the title of the book,
- the {% role %}People Care/Facility{% endrole %} will buy the book, depending on availability of office budget.
- help to maintain the internal library. Please check what action is required for physical books on the [Library](Library.md) page.

In Agile Lab we have a digital library. 

If you want to buy a new digital book that is also DRM-free,
- follow the [purchasing process](#purchasing-process) below,
- help to maintain the internal library. Please check what action is required for digital books on the [Library](Library.md) page.



**Coffee**

Coffee is free !!!


# Expenses

This is mainly referred to work trip expenses.
Please arrange yourself the best solution. Don't ask {% role %}People Care/Corporate Travel Officer{% endrole %} to organize the travel for you, {% role %}People Care/Corporate Travel Officer{% endrole %} will only pay for it.
If you have time to plan a trip, do it in advance to reduce costs as you do with your personal trips.
You don't need authorizations unless you have to bypass soft limits, so don't ask for them.


**Car**

If you use your personal car for a work trip, the mileage ( KM ) is reimbursed according to local law. 
If you don't own a car and you think the best solution is to rent one, just do it.


**Launch/Dinner**

No limits. ( Spend company money like it is your own money )


**Hotel**

Soft limit at 100 Euro/Night. 
If you need to bypass the soft limit, ask for approval from the owner of the budget you are using (e.g., Lead Link of the Circle you are traveling for)


**Transportations**

Soft limit is 500 Euro overall for transportations.
If you need to bypass the soft limit, ask for approval from the owner of the budget you are using (e.g., Lead Link of the Circle you are traveling for)

**Conferences**

Soft limits described above are for generic work trips, not conferences. Conferences have different spending criteria, please refer to the [training budget benefit](Benefits.md#conferences) section.

# Purchasing process

## Scenario 1: self-purchase and reimbursement request

1. buy the good you need (obviously referring to the current guidelines for [Benefits](Benefits.md), and the other mentioned above)
2. add a new expense entry on Elapseit with:
    1. *type* : `07) RIMBORSO PIE' DI LISTA`
    2. *Project* :  it should depend on what the expense is related to. Imagine it's time instead of goods and use the same guidelines of projects as in [ProjectTimesheet](ProjectTimesheet.md). E.g. if it's personal training material, you must use `<current year> Agile - Training - Learning`.
    3. compile the other fields as usual (incurred date, actual currency, amount) and attach the receipt document
    4. submit the entry
3. the administration will include the reimbursement in the next payroll

## Scenario 2: request the Buyer to purchase something

1. find the good you need and verify if your personal budget allows this purchase (obviously referring to the current guidelines for [Benefits](Benefits.md), and the other mentioned above)
2. add a new expense entry on Elapseit with:
    1. *type* : `20) PURCHASED BY BUYER (RDA - RICHIESTA DI ACQUISTO)`
    2. *Project* :  it should depend on what the expense is related to. Imagine it's time instead of goods and use the same guidelines of projects as in [ProjectTimesheet](ProjectTimesheet.md). E.g. if it's personal training material, you must use `<current year> Agile - Training - Learning`.
    3. compile the other fields as usual (incurred date, actual currency, amount)
    4. submit the entry
    5. once submitted, get the Elapseit's _"Expense ID"_ (e.g. `ELI_0001234`)
3. email the Buyer of your choice, or buyer@agilelab.it, including:
    1. the link (and, if necessary, the instructions) for the purchase
    2. the Expense ID you got from point 2.5  (VERY IMPORTANT!!!)
4. if approved, the Buyer proceeds with the purchase and add by himself/herself the receipt document editing the Elapseit entry, leveraging the Expense ID to find it. This closes the process and the expense is trackable by everybody.

Additionally, the `Is billable` checkbox is reserved for expenses that can be billed on the customer and in the scope of a specific project (i.e. work trip to the customer's offices); this option is to be left unchecked in all other cases.

Following this process, you can easily track your personal training budget status, lead links can autonomously track the circle's expenses and administration processes will be more efficient.


