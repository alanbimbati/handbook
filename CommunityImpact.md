# Data Engineering Community Impact

This circle purpose is:

> _"Create an impact on data engineering community at worldwide level"._

This derives from the global purpose of _"... elevating the Data Engineering game"_. This circle operates as an enabling team. Find more info about what this means on the [Team Topologies](TeamTopologies.md) page.

As described in the [principles and pillars page](DataEngineering.md), Elite Data Engineering is core for us, thus we would like to also bring our vision, expertise, and contribution to the worldwide community.

# Join the circle activities

**Ready to make an impact?**

Join the Data Engineering community in shaping our future with open source contributions, writing insightful articles, taking the stage as a speaker, and even lending a hand in organizing events. Your journey starts by reaching out to the respective role holders. Let's elevate our collective potential!


The main activities carried out by the circle are:

1. Open Source contributions: see the [relevant section](#open-source-contributions)
1. Creation of [knowledge base contents](https://www.agilelab.it/blog/tag/knowledge-base) and articles: see the [relevant section](#writing)
1. Public speaking: spread "elite data engineering at conferences and meetups around the world": see the [relevant section](#speaking)
1. Actively participate to activities of global data engineering/management related communities: see the [relevant section](#organizing)

## Open Source contributions

We sponsor open source contributions: in accordance with your Tech Lead/BU Lead and with the designated {% role %}Data Engineering community impact/Open Source Contributor{% endrole %} you can carve out company time to contribute to Open Source projects, being them internal or external. If you have an idea or feel that you can contribute, initiate the process by pinging the Open Source contributor of your choice.

More details can be found in the [dedicated section](OpenSource.md).

## Writing

We sponsor, with company time and coaching, technical writers. Any technical matter can be the subject of an article that {% role %}Data Engineering community impact/Chiara Ferragni{% endrole %} will take care of reviewing and help you publish following the process and the [guidelines defined by Marketing](mktg_articles.md).

## Speaking

We sponsor with company time (1 day during the conference, 16 hours of preparation), coaching (as much as you need), and also cover the transport and stay costs (for as long as it's needed, according to the remaining budget, in first-come-first-served fashion) public speaking opportunities. 
Becoming a speaker is easier than you think, start with applying to a "call for presentation" at a conference you'd like to participate. When you are admitted/selected, contact {% role %}Data Engineering community impact/Chiara Ferragni{% endrole %} who will validate your sponsorship and help you to create a beautiful presentation.

The only limitation to the participation as a speaker at conferences is the `speaker budget` available in this circle. This means you have the opportunity to participate as a speaker with `speaker budget` funding *any* conference participation even multiple times, until the budget runs out. The circle will try to balance requests if many people apply based on conference relevance and speakers (i.e. we're not sending 1 people to 5 conferences if that means running out of budget and precluding the opportunity to other people). **This `speaker budget` does not interfere with your [training budget](Benefits.md#training-budget).**


> _When you're participating at a conference as a speaker, take the chance to help organizers, moderating round tables, for example._

## Organizing

Helping community managers and event organizers can help guide the event and make it more successful. If you feel like you can be of any help to any relevant event or community, please touch base with {% role %}Data Engineering community impact/Indiana Jones{% endrole %} who will evaluate your request and guide you.