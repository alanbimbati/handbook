SCOPE
Define the procedures for managing operational continuity and disaster recovery.

DESCRIPTION OF ACTIVITIES
2.1 OPERATIONAL IMPACT ANALYSIS AND RISK ASSESSMENT

The information collected through operational impact analysis and risk assessment forms the foundation for an effective operational continuity management program.

In conducting the Business Impact Analysis (BIA) and risk assessment process, the company defines risk scenarios, which are tools for predicting possible damages and the resulting impact on the organization.

These risk scenarios are defined based on knowledge of the context, vulnerabilities, and reference events that are considered more likely to occur.

The assessment of these risk scenarios, which aims to provide an immediate evaluation of losses, is crucial for defining the Business Continuity Plan.

The company achieves its objectives by delivering services to stakeholders. Therefore, it is important to understand the negative impact over time that the failure to provide these products or services and associated activities could have on the organization's objectives and operations.

The BIA, which maps critical business processes (where critical does not refer to less-performing processes, but rather to those that are most important or core to the business and whose failure to be restored within the defined objectives could compromise the organization's survival), identifies their level of criticality, the impact on the business, the recovery timeframe (recovery requirements), and the resources needed to restore and maintain the service at acceptable levels.

Based on the BIA, the company defines specific indicators such as:

Maximum Acceptable Outage (MAO)
Maximum Tolerable Period of Disruption (MTPD)
Minimum Business Continuity Objective (MBCO)
Recovery Point Objective (RPO)
Recovery Time Objective (RTO)
These indicators are necessary to define the Business Continuity Strategy and the Business Continuity Plan.

In addition to the BIA, the organization establishes, implements, and maintains a formal and documented risk assessment process that systematically identifies, analyzes, and evaluates the risks of disaster incidents for the organization.

2.2 OPERATIONAL CONTINUITY STRATEGY

The determination of the operational continuity strategy involves identifying the actions necessary to control the results of the operational impact analysis and risk assessment, in order to meet the organization's operational continuity objectives.

The determination and selection of an operational continuity strategy are based on the outcomes of the operational impact analysis and risk assessment.

The consolidation, continuation, recovery, and restoration of priority activities consider other related activities and their support resources.

The Business Continuity Strategy determines:

Protection of priority activities
Stabilization, continuation, recovery, and restoration of priority activities and their dependencies
Mitigation, by responding to the impact through management actions
The determination of the strategy includes prioritizing the timeframe for activity recovery.

The company also evaluates the Business Continuity capabilities of suppliers involved in its business processes.

2.3 BUSINESS CONTINUITY PLAN

The main contents of the Business Continuity Plan include:

Purpose and scope
Effectiveness response evaluation criteria
Response activation criteria
Response procedures
Crisis management responsibilities
Communications
Interfaces
Resources
Information flows
The Business Continuity Plan also includes the Disaster Recovery Plan.

Specifically, in the Business Continuity Plan, the company verifies:

Roles, responsibilities, and authorities for each documented procedure/activity
Incident management and crisis declaration
Contact information within each documented procedure
Communication methods
Implementation procedures, identifying the actions and tasks to be executed
The company reviews the procedures for recovering operational activities. The goal of recovery is to resume operational activities that support the normal business operations following a disruptive event.

Return to normalcy can be achieved through:

Repairing damages resulting from the incident
Migrating activities from temporary locations to the main recovered location
Relocating to a new location
2.4 DISASTER RECOVERY PLAN

The Disaster Recovery Plan (DR) details the necessary steps for restoring the resources (hardware, software, infrastructure, etc.) used for service delivery by the company.

The DR plan provides detailed operational procedures to correctly assess the emergency/disaster situation that prevents the normal delivery of services by the organization.

The document includes the various phases required to restore the telecommunications system, recover data, and configure procedures to address the emergency and initiate the subsequent return to normal operational conditions.

2.5 EXERCISES AND TESTING

The organization's continuity procedures and related preparations cannot be considered reliable until they have been verified through exercises and ensured to be updated.

Exercising is essential to ensure that strategies, policies, plans, and procedures that have been implemented are adequate and can meet the objectives of operational continuity. Exercises promote teamwork, competency, confidence, and knowledge and involve those who may need to use the procedures.

Elements of exercises and testing include:

Exercise program
Exercises related to Business Continuity plans
The company has designed scenarios that meet the objectives of the exercises, using threats identified in the risk analysis or other appropriate events.

The company has determined:

Annual exercise program
Reports on conducted exercises with evaluations of the outcomes and analysis of any deviations from expectations.