# Rende

## How to reach us
- Address: [Talent Garden Via Venezia, 24, 87036 Rende CS](https://goo.gl/maps/ZF2oAb6yrhVnjwqq5)
    - [Website](https://talentgarden.org/it/coworking/italy/cosenza/)
- Parking: Free
- By bus:
    * Local Bus [link](https://moovitapp.com/cosenza-3280/poi/Via%20Trieste%204/Cosenza/it?ref=2&customerId=4908&tll=39.324256_16.244154&fll=39.298257_16.253833) and then a 10 minute walk

## Office
- Spaces:
  * _Operative area_: 5 desks in an open-space, fully equipped with external screens, keyboards, mouses, wired and wi-fi connection
  * 1 conference room, also usable for private calls and meetings (to be booked in advance - please refer to the Rende {% role %}People Care/Facility{% endrole %} for instructions)
  * 1 event space (to be booked in advance - please refer to the Rende {% role %}People Care/Facility{% endrole %} for instructions) 
  * _Recreative area_: coffee zone, table football
  * _Relax area_ : sofa, treadmill
